# insitu

Code accompanying the paper:

H. Brönnimann, J. Iacono, J. Katajainen, P. Morin, J. Morrison, and G. T. Toussaint.
Space-efficient planar convex hull algorithms.
Theoretical Computer Science, 321(1):25-40, 2004.
Special issue of selected papers from Latin American Theoretical INformatics (LATIN 2002).

Available at:

http://cglab.ca/~morin/publications/insitu/insitu-src.tgz

under unknown license.

This is NOT my work. It's uploaded here to ease submodule management in [xgrid3d](https://github.com/seirios/xgrid3d/). Credit is due to the authors.
